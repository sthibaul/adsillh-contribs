mdfiles=contribs.md $(shell find . \( -name "*.md" ! -name contribs.md \) | sort -r)

contribs.html: *.md
	pandoc -f markdown -t html -o $@ ${mdfiles}

clean:
	rm -f contribs.html
